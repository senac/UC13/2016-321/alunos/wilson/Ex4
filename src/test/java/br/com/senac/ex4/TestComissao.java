/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.ex4;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author Eletronicaras
 */
public class TestComissao {

    @Test
    public void deveCalcularComissao() {

        CalculadoraComissao calculadora = new CalculadoraComissao();

        double resultado = calculadora.calcular(new Venda(25, 8));
        assertEquals(10, resultado, 0.001);

    }

}
