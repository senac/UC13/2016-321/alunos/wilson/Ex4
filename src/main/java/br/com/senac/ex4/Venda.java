/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.ex4;

/**
 *
 * @author Eletronicaras
 */
public class Venda {

    private double precoUnitario;
    private double quantidadeVendida;

    public Venda() {
    }

    public Venda(double precoUnitario, double quantidadeVendida) {
        this.precoUnitario = precoUnitario;
        this.quantidadeVendida = quantidadeVendida;
    }

    public double getPrecoUnitario() {
        return precoUnitario;
    }

    public void setPrecoUnitario(double precoUnitario) {
        this.precoUnitario = precoUnitario;
    }

    public double getQuantidadeVendida() {
        return quantidadeVendida;
    }

    public void setQuantidadeVendida(double quantidadeVendida) {
        this.quantidadeVendida = quantidadeVendida;
    }

}
