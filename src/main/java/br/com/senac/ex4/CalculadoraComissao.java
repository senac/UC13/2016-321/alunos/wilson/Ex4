/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.ex4;

/**
 *
 * @author Administrador
 */
public class CalculadoraComissao {
    
    
    public double calcular(Venda v) {

        double valorFinal = v.getQuantidadeVendida() * v.getPrecoUnitario();
        double comissao = valorFinal * 0.05;

        return comissao;
    }
    
}
